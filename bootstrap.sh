#!/bin/bash

ensure_root () {
    if [[ "$EUID" -ne 0 ]]; then
	exec sudo "$0" "$@"
    fi
}

install_deps () {
    export DEBIAN_FRONTEND=noninteractive
    apt-get update && apt-get -y dist-upgrade

    apt-get install -y rsync git nginx avahi-utils
    apt-get install -y net-tools

    # Django-related Debian packages
    apt-get install -y python3-dev python3-pip python3-venv
    apt-get install -y python3-ldap python3-django-python3-ldap libldap2-dev libsasl2-dev
    apt-get install -y fuse


    # Postgres packages
    apt-get install -y postgresql-11 vim

    # LDAP
    apt-get install -y slapd ldap-utils ldapscripts redis

    apt-get install -y systemd

}

configure_deps () {
    postgresconf=/etc/postgresql/11/main/postgresql.conf
    grep '^listen_addresses' $postgresconf || echo "listen_addresses = 'localhost'" >>$postgresconf

    ## configure slapd
    LDAP_ROOTPASS=admin
    LDAP_DOMAIN=host.iuvia.io
    LDAP_DOMAIN_DC=dc=host,dc=iuvia,dc=io
    LDAP_ORGANISATION="IUVIA Device"

    cat <<EOF | debconf-set-selections
slapd slapd/password1 password ${LDAP_ROOTPASS}
slapd slapd/password2 password ${LDAP_ROOTPASS}
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
slapd slapd/domain string ${LDAP_DOMAIN}
slapd shared/organization string ${LDAP_ORGANISATION}
slapd slapd/backend string MDB
slapd slapd/purge_database boolean true
slapd slapd/move_old_database boolean true
slapd slapd/allow_ldap_v2 boolean false
slapd slapd/no_configuration boolean false
slapd slapd/dump_database select when needed
EOF

    dpkg-reconfigure slapd

    BOXUI_DB_PW=$(pwgen -C 6 4)
    BOXUI_DB_NAME=boxui_prod

    # Does boxui user exist?
    PGRESULT=$(sudo -u postgres psql -c "SELECT * FROM pg_catalog.pg_user WHERE usename='boxui'")
    if echo "$PGRESULT" | grep '(0 rows)'; then
	# User does not exist yet
	sudo -u postgres psql -c "CREATE ROLE boxui with PASSWORD '$BOXUI_DB_PW' LOGIN"
    else
	sudo -u postgres psql -c "ALTER USER boxui with PASSWORD '$BOXUI_DB_PW'"
    fi

    PGRESULT=$(sudo -u postgres psql -c "CREATE DATABASE ${BOXUI_DB_NAME}")
    PGRESULT=$(sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE ${BOXUI_DB_NAME} TO boxui")

    DJANGO_SECRET_KEY=$(pwgen 64 1)
    DJANGO_DEBUG=True

    cat >/etc/systemd/system/boxui.envfile <<EOF
DATABASE_URL=postgres://boxui:${BOXUI_DB_PW}@localhost/${BOXUI_DB_NAME}
REDIS_URL=redis://127.0.0.1:6379/
CELERY_BROKER_URL=redis://127.0.0.1:6379/
APPIMAGE_DIR=/var/lib/iuvia/appimages/downloaded
IUVIA_APPIMAGE_LOCAL_DATA_PATH=/var/lib/iuvia/appimages/downloaded
IUVIA_BOXUI_DATA_PATH=/var/lib/iuvia/boxui
DJANGO_SECRET_KEY=${DJANGO_SECRET_KEY}
DJANGO_DEBUG=${DJANGO_DEBUG}
SITE_NAME=iuvia
LDAP_BIND_SEARCH=${LDAP_DOMAIN_DC}
LDAP_URI=ldap://localhost:389
LDAP_START_TLS=False
LDAP_BIND_DN=cn=admin,${LDAP_DOMAIN_DC}
LDAP_BIND_PASS=${LDAP_ROOTPASS}

USE_DOCKER=False
EOF

}


retrieve_boxui () {

    BOXUI_LOCATION=
    BOXUI_BRANCH=master

    if [[ -d '/mnt/boxui' ]]; then
	BOXUI_LOCATION=/mnt/boxui
	BOXUI_TMP=0
    else
	BOXUI_LOCATION=`mktemp -d`
	git clone --recurse-submodules --shallow-submodules --depth=1 --branch=$BOXUI_BRANCH https://gitlab.com/thedrastical/iuvia/boxui.git "$BOXUI_LOCATION"
	BOXUI_TMP=1
    fi

    # Sync /mnt/boxui if we are not in Vagrant.
    # If we are in Vagrant, we have a Vagrantfile that rsyncs things (you can run vagrant rsync-auto to update continuously)
    [[ -n "$INSIDE_VAGRANT" ]] || rsync -rvpP --exclude=venv --exclude=.git/objects --exclude=.tox "$BOXUI_LOCATION/" /opt/boxui/

    [[ -z "$INSIDE_VAGRANT" ]] || mkdir -p /opt
    [[ -z "$INSIDE_VAGRANT" ]] || ln -s /vagrant/vendor/boxui /opt/boxui

    [[ -z "$INSIDE_VAGRANT" ]] || mkdir -p /var/lib/iuvia/appimages/
    [[ -z "$INSIDE_VAGRANT" ]] || ln -s /vagrant/vendor/appimage-repository/appimages /var/lib/iuvia/appimages/downloaded

    if [[ $BOXUI_TMP == '1' ]]; then
	rm -rf BOXUI_TMP
    fi

}

install_boxui () {
    pip3 install -e '/opt/boxui/[dev]'
    boxui-systemd-service install

    mkdir -p /run/services/http

    rm -rf /etc/nginx/sites-enabled/*

    # TODO: Remove apt cache from the image
    # rm -rf /var/lib/apt/lists

}

install_deps_platform_php () {
    ## Install dependencies of Nextcloud as part of the platform for the moment
    PHP_EXT_DEPS='exif gd intl ldap opcache mysql pgsql zip gmp xml mbstring sqlite'
    PHP_PACKAGES='php7.3'
    for ext in $PHP_EXT_DEPS; do
	PHP_PACKAGES="$PHP_PACKAGES php7.3-$ext"
    done
    apt-get install -y --no-install-recommends apache2 $PHP_PACKAGES
    systemctl disable --now apache2.service # very important
}

boxui_migrate () {
    # systemd-run --unit=boxui-migrate.service --property=EnvironmentFile=/etc/systemd/system/boxui.envfile -t python3 /opt/boxui/manage.py migrate

    echo "I cannot create the superuser automatically. Please run the following command after provisioning"
    echo systemd-run --property=EnvironmentFile=/etc/systemd/system/boxui.envfile -t python3 /opt/boxui/manage.py createsuperuser
}

start_boxui () {
    systemctl enable --now boxui.socket
    systemctl enable --now iuvia.control.socket
    python3 -m iuvia.control.client register_service_demo
}

ensure_root
install_deps
configure_deps
retrieve_boxui
install_boxui
install_deps_platform_php

boxui_migrate
start_boxui
