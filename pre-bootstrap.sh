#!/bin/bash
CHUBASCOS_DEV_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

BOXUI_REPO="git@gitlab.com:iuvia/boxui.git"
APPIMAGE_REPO="git@gitlab.com:iuvia/marketplace/appimage-main.git"

cd "$CHUBASCOS_DEV_FOLDER"

mkdir -p vendor

[[ -d vendor/boxui ]] || git clone "$BOXUI_REPO" vendor/boxui
(cd vendor/boxui && pwd && ls && bash build_frontend_production.sh)

[[ -d vendor/appimage-repository ]] || git clone "$APPIMAGE_REPO" vendor/appimage-repository

