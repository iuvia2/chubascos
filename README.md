# ChubascOS

This repository contains a customized Debian installation that will be used as a base for iuvia.

# Vagrant + VirtualBox image

The Vagrantfile exposes a Debian Buster image. 

Beware that old versions of this repo and of debian/buster64 (v10.0.0) were not not that easy to set up.

Currently, in order to set up for the first time, you need to do the following:

    $ bash pre-bootstrap.sh
    $ vagrant up

As you would normally do.


This normally sets up your working folder as `/vagrant` in the guest machine via the vboxfs.

If you prefer to use other synching mechanisms (nfs, rsync, or others), you can refer to the Vagrant documentation.

### Development workflow

When upgrading BoxUI to a different version (in vendor/boxui), you need to do
the following command to stop the old BoxUI instance:

```
vagrant ssh sudo systemctl stop boxui.service
```

The new version will be started automatically from the `boxui.socket` on the
first connection.

### Known issues

#### When you change files that are monitored with rsync-auto boxui is no longer installed

You may notice that when you change files in boxui while they are
rsync'd, the application is no longer installed, and several
utilities, like for example, boxui-systemd-service may fail raising a
`DistributionNotFound` exception.

The current solution is to reinstall the app, but plase modify this if
a better solution is found.

Reinstallation should be done with

```bash
sudo pip3 install -e /opt/boxui[dev]
```

so that things are not copied twice onto the Vagrant machine.

